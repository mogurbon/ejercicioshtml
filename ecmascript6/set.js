//ES5

var myObjSet = {'chris': true, 'derek': true, 'tony': true};
console.log(Object.keys(myObjSet)); // 'chris', 'derek', 'tony'


//ES6

let mySet = new Set();
mySet.add('chris');
mySet.add('derek');
mySet.add('tony');
console.log(mySet.keys()); // 'chris', 'derek', 'tony'


let mySet = new Set([1]);
console.log(mySet); //Set {1}
mySet.add(1); // ya existe, lo ignora
console.log(mySet); //Set {1}
mySet.add('1'); // tipo diferente
console.log(mySet);// Set {1, "1"}
mySet.add({foo: 'bar'});
console.log(mySet); //Set {1, "1", Object {foo: "bar"}}
mySet.add({foo: 'bar'}); // referencia de objeto diferente
console.log(mySet); //Set {1, "1", Object {foo: "bar"}, Object {foo: "bar"}}