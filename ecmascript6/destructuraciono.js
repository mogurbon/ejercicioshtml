const items = [
    { word:'foo', count:1 },
    { word:'bar', count:3 },
];
items.forEach(({word, count}) => {
    console.log([word,' ',count].join(''));
});