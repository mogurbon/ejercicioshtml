class FlightRobot{
  drive() {
    return "I'm moving";
  }
}

class CleaningRobot extends FlightRobot{
	clean(){
		return "I'm cleaning";
	}
}

const wally = new FlightRobot();
console.log(wally.drive()); // I'm moving

const cleanner = new CleaningRobot();
console.log(cleanner.drive()); // I'm moving
console.log(cleanner.clean()); // I'm cleaning