function Animal() {
   this.mammal = true;
}
Animal.prototype.bark= function() {
  return 'woof';
};
Animal.prototype.meow = function() {
  return 'meau';
};

const dog = new Animal();
console.log(dog.bark()); // "woof"
const cat = new Animal();
console.log(cat.meow()); // "meau"




class Animal {
  constructor() {
    this.mammal = true;
  }
  bark() {
    return "woof"; 
  }
  meow() {
    return "meau"; 
  }
}
const firulais = new Dog();
firulais.talk() // "woof"