//ES5

function print (first, last) {
  const msg = 'Welcome ' + first + ' ' + last + '!';
  // teniendo en cuenta que en nuestro HTML tenemos un tag con id igual a msg
  // por ejemplo <h2 id="msg"></h2>
  document.getElementById('msg').innerHTML = msg;
}