// Los array implementan por defecto el patrón 
let mylist = [1, 2 ,3];

for (let myitem of mylist) {
    console.log(myitem);
}

// Devuelve = ['1', '2', '3']