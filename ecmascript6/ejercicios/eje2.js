var versionsMap = {};
lines.forEach(line => {

  // split name@version into [name, version]
  let lineContents = line.split('@');
  let[name,version ]= lineContents;
  versionsMap[name] = version;
});
