function Hello(name) {
  this.name = name;
}

Hello.prototype.hello = function hello() {
  return 'Hello ' + this.name + '!';
};

Hello.sayHelloAll = function () {
  return 'Hello everyone!';
};

function HelloWorld() {
  Hello.call(this, 'World');
}

HelloWorld.prototype = Object.create(Hello.prototype);
HelloWorld.prototype.constructor = HelloWorld;
HelloWorld.sayHelloAll = Hello.sayHelloAll;

HelloWorld.prototype.echo = function echo() {
  alert(Hello.prototype.hello.call(this));
};

var hw = new HelloWorld();
hw.echo();

alert(Hello.sayHelloAll());





class HelloWorld {
  constructor(name) {
    this.HelloWorld();
  }
  Hello (name){
  	this.name=name;
  }
  hello() {
     return`Hello ${this.name}!`;
  }
  HelloWorld(){
  	this.Hello('World');
  }
  echo (){
  	alert (this.hello());
  }
  sayHelloAll(){
  	return 'Hello everyone!';
  }
}

var hw = new HelloWorld();
hw.echo();

alert(Hello.sayHelloAll());












