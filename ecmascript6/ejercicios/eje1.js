Template.stats.onCreated( () =>{
  let self = this;
  this.autorun(() =>{
    let filter = { limit: Session.get('options.limit') };
    self.subscribe('tweets', filter);
  });

});
