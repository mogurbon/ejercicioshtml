const voltea = cadena => cadena.split("").reverse().join("");
class LN {
  constructor() {
    this.lista= [];
  }
  insert(item) {
    this.lista.push(item);
  }
  search(searched_item){
  	return this.lista.find( item => item === searched_item);
  }
  palindromo(){
  	return this.lista.filter( item =>  item == voltea(item));

  }
  print(){
  	return this.lista;
  }
}
const lista = new LN();
lista.insert("anitalavalatina");
lista.insert("prueba");
lista.insert("madam");
lista.insert("lista");
lista.insert("prueba2");
lista.insert("react");
console.log(lista.print());
console.log(lista.palindromo());
console.log(lista.search("react"));

