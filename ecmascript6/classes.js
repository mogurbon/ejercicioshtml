class Dog {
  constructor() {
    this.sound = 'woof';
  }
  talk() {
    console.log(this.sound);
  }
}
const firulais = new Dog();
firulais.talk() // "woof"