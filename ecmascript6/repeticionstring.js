//ES5

String.prototype.repeat = function (num) {
  return new Array(num + 1).join(this)
}
console.log('foo'.repeat(4)); // foofoofoofoo


//ES6
console.log('foo'.repeat(4)); // foofoofoofoo