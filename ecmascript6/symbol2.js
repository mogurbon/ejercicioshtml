const foo = Symbol()
const bar = Symbol()

typeof foo === "symbol" //true
typeof bar === "symbol" //true



let obj = {}

obj[foo] = "foo"
obj[bar] = "bar"    

JSON.stringify(obj) // {}
Object.keys(obj) // []

Object.getOwnPropertyNames(obj) // []
Object.getOwnPropertySymbols(obj) // [ foo, bar ]

console.log(obj);
console.log(Object.getOwnPropertySymbols(obj))