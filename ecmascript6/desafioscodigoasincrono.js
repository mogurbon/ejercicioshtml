const el = document.querySelector('.btn');
el.addEventListener('click', function() {
}, false)
//manejo de error
getData(function(data){
}, function(error){
});

//combinacion de callbacks
getUser('user', function (user) {
     getFriendsForUser(user.name, function(friends) {
         getAvatar(friends[0], function(avatar) {
             showAvatar(avatar);
         });
     });
});


//logica para asegurar finalizacion de algun llamado
function showUserAndFriend() {  
   let finishedSoFar = 0;

   function somethingFinished() {
      if (++finishedSoFar === 2) {
          hideSpinner();
      }
   }

   getUser('user', function(user) {
      showUser(user);
      somethingFinished();
   });

   getFriends('user', function(friends) {
     showFriend(friends[0]);
     somethingFinished();
   });
}



//mas errores

getUser('user', function(err, user) {
  if (err) {
    ui.showError(err);
  } else {
    getFriendsForUser(user, function(err, friends) {
      if (err) {
        ui.showError(err);
      } else {
        ui.showFriends(friends);
      }
    });
  }
});