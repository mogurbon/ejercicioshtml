new Intl.NumberFormat([locales][, options])



let number = 3500;
// Si no especificamos el parámetro "locale", el formato del lenguaje será el usado por defecto en la interfaz de usuario de nuestra aplicación
console.log(new Intl.NumberFormat().format(number));
// → '3,500' en caso que nuestro "locale" por defecto sea US English


let number = 123456.789;
// El idioma Alemán usa la coma para separar decimales y punto para separar milésimas.

console.log(new Intl.NumberFormat('de-DE').format(number));
// → 123.456,789