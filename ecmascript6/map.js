//ES5
var obj = {};
obj[0] = 'chris';
obj[1] = 'derek';
obj[2] = 'tony';

console.log(obj); // Object {0: "chris", 1: "derek", 2: "tony"}
console.log(Object.keys(obj).length); //3
for (var key in obj) { //iteración sobre un objeto
   if (obj.hasOwnProperty(key)) {
      console.log(key + ':' + obj[key]);
   }
}

//ES6

let map = new Map();
map.set(0, 'chris');
map.set(1, 'derek');
map.set(2, 'tony');

console.log(map); // Map {0 => "chris", 1 => "derek", 2 => "tony"}
console.log(map.size); //3
for (const [key, value] of map) { //iteración sobre un Map
  console.log(key + ':' + value);
}