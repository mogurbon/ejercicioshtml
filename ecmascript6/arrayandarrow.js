//ES5

var result = values.sort(function(a, b) {
    return a - b;
});

//ES6
var result = values.sort((a, b) => a - b);

