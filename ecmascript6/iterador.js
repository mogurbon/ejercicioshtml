// Esto es análogo a definir [1, 2, 3]
var mylist = {}
mylist[Symbol.iterator] = function () {
    let contador = 0;
  		return {
        next: function () {
        contador++;
            if (contador <= 3) {
                return {value: contador, done: false};
            } else {
                return {done: true};
            }    
        }  
    }
};
for (let myitem of mylist) {
    console.log(myitem);
}
// Devuelve = ['1', '2', '3']