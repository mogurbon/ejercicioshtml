Array.prototype.keys()
Array.prototype.values()
Array.prototype.entries()


//ES5
var names = ['chris', 'derek', 'tony'];

console.log(Object.keys(names)); // ['0', '1', '2']


//ES6

let names = ['chris', 'derek', 'tony'];
let entries = names.keys();

console.log(entries.next().value); // 0
console.log(entries.next().value); // 1
console.log(entries.next().value); // 2
console.log(entries.next().done); // true
console.log([...names.keys()]); // [0, 1, 2]


let names = ['chris', 'derek', 'tony'];
let entries = names.values();

console.log(entries.next().value); // chris
console.log(entries.next().value); // derek
console.log(entries.next().value); // tony
console.log(entries.next().done); // true
console.log([...names.values()]); // ['chris', 'derek', 'tony']



let names = ['chris', 'derek', 'tony'];
let entries = names.entries();

console.log(entries.next().value); // [0, 'chris']
console.log(entries.next().value); // [1, 'derek']
console.log(entries.next().value); // [2, 'tony']
console.log(entries.next().done); // true
console.log([...names.entries()]); // [[0, 'chris'], [1, 'derek'], [2, 'tony']]