//ES5
var person = function (name) {
  return {
    getName: function() {
      return name;
    }
  }
}("Luis Miguel");

console.log(person.getName()); // <- Luis Miguel   


//ES6
let person = ((name) => {
  return {
    getName: function() {
      return name;
    }
  }
})("Luis Miguel");

console.log(person.getName()); // <- Luis Miguel