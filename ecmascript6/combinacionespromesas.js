Promise.all([
  getFriendsForUser('user'),
  getLikedpagesByUser('user')
]).then(function(results) {
  //Ambas promesas fueron resueltas
});

Promise.race([
  getFriendsForUser('user'),
  getLikedpagesByUser('user')
]).then(function(results) {
  //al menos una promesa fue resuelta
});