class LinkedList {

 constructor() {
   this.head = null;
   this.tail = null;
   this.count = 0;
 }

 getLength () {
   return this.count;
 }

 insert(elem, after) {
   const node = {
     data: elem,
     next: after
   };

   const temp = this.head;
   this.head = node;
   this.head.next = temp;
   this.count++;

   if (this.count === 1) {
     this.tail = this.head;
   }
 }

 print() {
   return this.head;
 }

 search( elem ) {
   let search = this.head;
   while(elem.name !== search.name ) {
     search = this.head.next;
   }
   return search;
 }

 isPalindrome() {
   let search;
   let str = this.head;
   let num = 0;
   let found = false;
   while ( !found ) {
     if (str == str.split('').reverse().join('')) {
       search = str;
       found = true;
     }
   }
   return str;
 }
}

var list = new LinkedList();
list.insert('anitalavalatina');

list.insert('prueba');
list.insert('lalala');

console.log(list.print());