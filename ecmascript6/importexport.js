//------ lib.js ------
export let counter = 3;
export function incCounter() {
    counter++;
}

//------ main.js ------
import { counter, incCounter } from './lib';

// El valor importado 'counter' está en vivo
console.log(counter); // 3
incCounter();
console.log(counter); // 4