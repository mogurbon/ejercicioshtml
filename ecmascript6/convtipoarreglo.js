//ES5
function cast () {
  return Array.prototype.slice.call(arguments)
}
console.log(cast('chris', 'derek', 'tony')); // ['chris', 'derek', 'tony']


//ES6

function cast () {
  return Array.from(arguments)
}

console.log(cast('chris', 'derek', 'tony')) // ['chris', 'derek', 'tony']