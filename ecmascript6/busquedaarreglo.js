Array.prototype.find()
Array.prototype.findIndex()


//ES5

var names = [{name: 'chris'}, {name: 'derek'}, {name: 'tony'}];

console.log(names.some(function(currentItem) {
  return currentItem.name === 'derek';
})); // true


//ES6
const names = [{name: 'chris'}, {name: 'derek'}, {name: 'tony'}];

console.log(names.find( currentItem => {
    return currentItem.name === 'derek';
})); // {name: 'derek'}



//ES5
var names = [{name: 'chris'}, {name: 'derek'}, {name: 'tony'}];
var namesAux = ['chris', 'derek', 'tony'];

console.log(names.indexOf({name: 'derek'})); // -1
console.log(namesAux.indexOf('derek')); // 1

//ES6
const names = [{name: 'chris'}, {name: 'derek'}, {name: 'tony'}];
const namesAux = ['chris', 'derek', 'tony'];

console.log(names.findIndex( currentItem => {
  return currentItem.name === 'derek';
})); // 1
console.log(namesAux.findIndex( currentItem => {
  return currentItem === 'derek';
})); // 1
