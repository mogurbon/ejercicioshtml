//ES5
var str = "Hello world";

if (str.indexOf("world") !== -1) {
  console.log("contains string world");
}

//ES6
var str = "Hello world";

str.includes(searchString[, position])

// El método includes() es sensible a mayúsculas o minúsculas
'Blue Whale'.includes('blue'); // false
'Blue Whale'.includes('Blue'); // true