class Robot {
  drive() {
    console.log("I'm moving")
  }
}

class CleaningRobot extends Robot {
  clean() {
    console.log("I'm cleaning")
  }
}

const wally = new CleaningRobot();
wally.drive(); // I'm moving