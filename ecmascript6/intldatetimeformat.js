let date = new Date("2015-01-02");

// El inglés de Estados Unidos usa el orden mes-día-año
console.log(new Intl.DateTimeFormat('en-US').format(date));
// → "1/2/2015"