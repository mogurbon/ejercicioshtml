function promedio(...numeros) {
  const suma = numeros.reduce(function(acc, value) {
    return acc + value;
  }, 0);
  return suma / numeros.length;
}