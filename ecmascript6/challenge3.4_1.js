let numero = 123456.789;

let formatJp = (numero) => {
  return new Intl.NumberFormat('ja-JP').format(numero);
};

console.log(formatJp(numero));