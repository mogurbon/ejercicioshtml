var allani ={ "animes":[{"name":"Saint Seiya", "description":"Jóvenes protectores de la diosa atena envestidos en miticas armaduras y regidos por constelaciones","img":"saint_seiya2.jpg", "genre":"shonen","edition":"normal"},
{"name":"Death Note","description":"Una libreta con el poder de matar a la persona con el nombre escrito en ella y un detective tratando de encontrar al escritor","img":"deathnote.jpg", "genre":"shonen","edition":"normal"},
{"name":"Naruto","description":"Un joven ninja Huerfano en busqueda de ser el mejor y convertirse en Hokage","img":"naruto.jpeg", "genre":"shonen","edition":"normal"},
{"name":"Mazinkaiser","description":"Un nuevo robot mas poderoso que mazinger Z en inclusive gran mazinger que ocasiona problemas con Koji al querer  manejarlo","img":"mazinkaiser.jpg", "genre":"shonen","edition":"anniversary"},
{"name":"Dragon Ball Super" ,"description":"Nuevas aventuras de Goku despues de haber derrotado a Majin buu","img":"dgbsuper.jpeg", "genre":"shonen","edition":"normal"},
{"name":"Oh My Goodess" , "description":"Que harias si tuvieras una diosa para ti","img":"ahmygoddess.jpg", "genre":"seinen","edition":"ovas"}] 
};
var topani ={ "animes":[{"name":"Saint Seiya", "description":"Jóvenes protectores de la diosa atena envestidos en miticas armaduras y regidos por constelaciones","img":"saint_seiya2.jpg"},
{"name":"Dragon Ball Super" ,"description":"Nuevas aventuras de Goku despues de haber derrotado a Majin buu","img":"dgbsuper.jpeg"}]
};

var recommendedani ={ "animes":[{"name":"Oh My Goodess" , "description":"Que harias si tuvieras una diosa para ti","img":"ahmygoddess.jpg"},
{"name":"Death Note","description":"Una libreta con el poder de matar a la persona con el nombre escrito en ella y un detective tratando de encontrar al escritor","img":"deathnote.jpg"},
{"name":"Mazinkaiser","description":"Un nuevo robot mas poderoso que mazinger Z en inclusive gran mazinger que ocasiona problemas con Koji al querer  manejarlo","img":"mazinkaiser.jpg"},
{"name":"Saint Seiya", "description":"Jóvenes protectores de la diosa atena envestidos en miticas armaduras y regidos por constelaciones","img":"saint_seiya2.jpg"}]
};

var qualiani ={ "animes":[
{"name":"Dragon Ball Super" ,"description":"Nuevas aventuras de Goku despues de haber derrotado a Majin buu","img":"dgbsuper.jpeg"}]
};
function get(animes){
	var cont =1;
	var html = '<div class="w3-row-padding">';
	
	for (anime in animes.animes){
		html += '<div class="w3-third w3-container w3-margin-bottom">';
		html += '<img src="imgs/'+animes.animes[anime].img+'" alt="Norway" style="width:100%" class="w3-hover-opacity">';
		html += '<div class="w3-container w3-white">';
		html += '<p><b>'+animes.animes[anime].name+'</b></p>';
		html += '<p>'+animes.animes[anime].description+'</p>';
		html += '</div>';
		html += '</div>';
		if ((cont % 3) ==0){
			 html += '</div>';
			 html += '<div class="w3-row-padding">';
    	}
		cont++;
	}
	document.getElementById("content").innerHTML = html;
	
}
function obtendatosusuario(user){
	var usuarios = JSON.parse(localStorage.getItem("usuarios"));
		for(u in usuarios.usuarios){
			if (usuarios.usuarios[u].usuario == user){
				var datosusuario = usuarios.usuarios[u];
			}
		}
		return datosusuario;
}

function getvalues(){
	user = getCookie("user");
	if (user != "") {
		var datosusuario;
		datosusuario=obtendatosusuario(user);
		var ad = "";
		if (datosusuario.admin==1) {ad ="Admin"}
		document.getElementById("username").innerHTML = datosusuario.usuario + ' '+ ad; 

		document.getElementById("userimage").src='imgs/'+ datosusuario.img;
	    get(allani);
	}
	else{
		window.location.href="login.html";
	}
}

function logout(){
	eraseCookie('user');
	location.reload();
}

 function search(){
 	var tosearch =document.getElementById("search").value;
 	if (tosearch!="") {
 		var arr=allani.animes.filter(function(a){return a.name.toLowerCase().includes(tosearch.toLowerCase()) || a.description.toLowerCase().includes(tosearch.toLowerCase()) }) ;
 		get({"animes":arr});
 	}
 }

 function usefilter(id){
 	var filterselected= document.getElementById(id).options[document.getElementById(id).selectedIndex].value;
	if (filterselected != id) {
		get({"animes":allani.animes.filter(function(a){return a[id] == filterselected})});
	}
 }